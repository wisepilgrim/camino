import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Modal,
  Image,
  Linking,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import { Colors, Metrics, Images, ApplicationStyles } from '../Themes';

export default class Popup extends Component<{}> {
  constructor(props){
    super(props);
    console.log(this.props.marker.image);
  }


  openURL = (url) => {
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        console.log("Don't know how to open URI: " + url);
      }
    });
  }

  render() {
    const { marker } = this.props;

    let showserviceview=(marker.accomtype=='Private' || marker.accomtype=='Municipal' || marker.accomtype=='Association' || marker.accomtype=='Parochial') ? true : false;
    let bookingsize=(marker.accomtype=='Private' || marker.accomtype=='Municipal' || marker.accomtype=='Association' || marker.accomtype=='Parochial' || marker.accomtype=='Camping') ? 'small' : 'big';

    //console.log(Images.images[marker.image]);
    return (
        <View style={styles.modalContent}>
          <TouchableOpacity
            style={{ flex: 0.1 }}
            onPress={() => this.props.navigator.dismissLightBox() }>
            <Image
                style={{ width: 24, height: 24, position: 'absolute', right: -10, top: -30 }}
                source={Images.popup.close} />
          </TouchableOpacity>

          <View style={styles.modalHeader}>
              <Text style={styles.headerTitle}>{marker.title}</Text>
              <Text style={styles.headerSubtitle}>{marker.accomtype}</Text>
          </View>
          { marker.image === undefined || marker.image === null ?
            null :
            <View style={styles.imgWrapper}>
              <Image style={styles.img}
              source={Images.images[marker.image]} />
            </View>
          }
          <View style={showserviceview ? styles.serviceView : {display: 'none'}}>
              <View style={styles.serviceIconGroup}>
                <Image style={marker.pilgrim ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.pilgrimBT} />
                <Image style={marker.hasprivate ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.hotelBT} />
                <Image style={marker.reserve ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.reserveBT} />
                <Image style={marker.barrest ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.barrestBT} />
                <Image style={marker.kitchen ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.kitchenBT} />
                <Image style={marker.family ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.familyBT} />
                <Image style={marker.veg ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.vegBT} />
                <Image style={marker.washer ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.washerBT} />
                <Image style={marker.wifi ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.wifiBT} />
                <Image style={marker.bike ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.bikeBT} />
              </View>
          </View>
          <View style={styles.practicalView}>
            <View style={marker.price ? styles.practicalIconGroup: {display: 'none'}}>
              <Image style={styles.practicalIcon}
                source={Images.popup.hostelBigBT} />
              <Text>{marker.beds}/{marker.price}</Text>
            </View>
            <View style={marker.open ? styles.practicalIconGroup: {display: 'none'}}>
              <Image style={styles.practicalIcon}
                source={Images.popup.openBT} />
              <Text>{marker.open}</Text>
            </View>
            <View style={marker.close ? styles.practicalIconGroup: {display: 'none'}}>
              <Image style={styles.practicalIcon}
                source={Images.popup.closeBT} />
              <Text>{marker.close}</Text>
            </View>
            <View style={marker.cal ? styles.practicalIconGroup: {display: 'none'}}>
              <Image style={styles.practicalIcon}
                source={Images.popup.calBT} />
              <Text>{marker.cal}</Text>

            </View>
            <TouchableOpacity style={((bookingsize=='small') && marker.bookingURL) ? styles.practicalIconGroup : {display: 'none'}}
              onPress={() => this.openURL(marker.bookingURL)}>
              <Image style={styles.bookingButtonSmall}
                source={Images.popup.bookingSmallBT} />
            </TouchableOpacity>

          </View>
          <TouchableOpacity style={((bookingsize=='big') && marker.bookingURL) ? styles.bookingmapbar : {display: 'none'}}
            onPress={() => this.openURL(marker.bookingURL)}>
            <Image style={styles.bookingButton}
              source={Images.popup.bookingBT} />
          </TouchableOpacity>
          <ScrollView contentContainerStyle={marker.body ? styles.description : { display: 'none'}}>
            <Text style={styles.descriptionText}>{marker.body}</Text>
          </ScrollView>
        </View>
    );
  }
}

const baseStyle = {
  width: '100%',
  height: 250
};

const styles = StyleSheet.create({
  modalContent: {
    width: Dimensions.get('window').width * 0.95,
    maxHeight: Dimensions.get('window').height * 0.85,
    backgroundColor: '#ffffff',
    borderRadius: 5,
    padding: 0,
  },
  modalHeader: {
    paddingTop: 0,
    paddingBottom: 5,
    flexDirection: 'column',
    alignItems: 'center',
    //backgroundColor: 'red',
    //marginTop: -10
    //position: 'absolute'
  },
  headerTitle: {
    fontSize: 16,
    fontWeight: 'bold',
    paddingLeft: 5,
    paddingRight: 5
  },
  headerSubtitle: {

  },
  img: {
    flex: 1,
    //paddingLeft: 20,
    height: null
  },
  img: baseStyle,
  imgWrapper: {
    overflow: 'hidden',
    ...baseStyle,
  },
  serviceView: {
    flexDirection: 'row',
    alignSelf: 'flex-start',
    padding: 4,
  },
  serviceIconGroup: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  serviceIcon: {
    height: 30,
    width: 30,
    marginRight: 0
  },
  serviceIconDisable: {
    height: 30,
    width: 30,
    marginRight: 0,
    opacity: 0.1
  },
  practicalView: {
    flex: 0,
    flexDirection: 'row',
    alignSelf: 'flex-start',
    padding: 4,
    justifyContent: 'space-between',
  },
  practicalIconGroup: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center'
  },
  practicalIcon: {
    height: 48,
    width: 48,
    marginRight: 0
  },
  bookingmapbar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 20
  },
  bookingButton: {
    height: 60,
    width: 60,
    marginRight: 5,
    marginLeft: 5,
    flex: 1
  },
  bookingButtonSmall: {
      height: 54,
      width: 54,
      marginRight: 5,
      marginLeft: 5,
  },
  description: {
    padding: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    //marginBottom: 60
  },
  descriptionText: {
    //flex: 1
  }
});

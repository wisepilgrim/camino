import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Modal,
  Image,
  Linking,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import { Colors, Metrics, Images, ApplicationStyles } from '../Themes';

export default class PopupOther extends Component<{}> {
  constructor(props){
    super(props);
    console.log("Img" + JSON.stringify(this.props.marker));

  }

  openURL = (url) => {
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        console.log("Don't know how to open URI: " + url);
      }
    });
  }

  render() {
    const { marker } = this.props;
    let tosantiago=(marker.tosantiago) ? (marker.tosantiago + 'km → Santiago') : '';
    let tohere=(marker.tohere) ? (marker.tohere + 'km → ') : '';
    //let tosantiago = marker.tosantiago;
    //let tosantiago = 999;
    let showserviceview=(marker.hasshare || marker.hasprivate  || marker.hascamping || marker.hospital || marker.pharmacy || marker.barrest || marker.supermarket || marker.atm || marker.correos || marker.busterminal || marker.bus || marker.train) ? true : false;
    return (
        <View style={styles.modalContent}>
          <TouchableOpacity
            style={{ flex: 0.1 }}
            onPress={() => this.props.navigator.dismissLightBox() }>
            <Image style={{ width: 24, height: 24, position: 'absolute', right: -10, top: -30 }}
              source={Images.popup.close} />
          </TouchableOpacity>

          <View style={styles.modalHeader}>
              <Text style={styles.headerTitle}>{tohere}{marker.title}</Text>
              <Text style={styles.headerSubtitle}>{tosantiago}</Text>
          </View>
          { marker.image === undefined || marker.image === null ?
            null :
            <View style={styles.imgWrapper}>
              <Image style={styles.img}
              source={Images.images[marker.image]} />
            </View>
          }

          <View style={showserviceview ? styles.serviceView : {display: 'none'}}>
              <View style={styles.serviceIconGroup}>
                <Image style={marker.hasshared ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.hostelBT} />
                <Image style={marker.hasprivate ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.hotelBT} />
                <Image style={marker.hascamping ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.campingBT} />
              </View>

              <View style={styles.serviceIconGroup}>
                <Image style={marker.hospital ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.hospitalBT} />
                <Image style={marker.pharmacy ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.pharmacyBT} />
              </View>

              <View style={styles.serviceIconGroup}>
                <Image style={marker.barrest ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.barrestBT} />
                <Image style={marker.supermarket ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.supermarketBT} />
                <Image style={marker.atm ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.atmBT} />
                <Image style={marker.correos ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.correosBT} />
              </View>

              <View style={styles.serviceIconGroup}>
                <Image style={marker.busterminal ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.busterminalBT} />
                <Image style={marker.bus ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.busBT} />
                <Image style={marker.train ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.trainBT} />
              </View>

          </View>
          <View style={styles.practicalView}>
            <View style={marker.price ? styles.practicalIconGroup: {display: 'none'}}>
              <Image style={styles.practicalIcon}
                source={Images.popup.hostelBigBT} />
              <Text>{marker.beds}/{marker.price}</Text>
            </View>
            <View style={marker.open ? styles.practicalIconGroup: {display: 'none'}}>
              <Image style={styles.practicalIcon}
                source={Images.popup.openBT} />
              <Text>{marker.open}</Text>
            </View>
            <View style={marker.close ? styles.practicalIconGroup: {display: 'none'}}>
              <Image style={styles.practicalIcon}
                source={Images.popup.closeBT} />
              <Text>{marker.close}</Text>
            </View>
            <View style={marker.cal ? styles.practicalIconGroup: {display: 'none'}}>
              <Image style={styles.practicalIcon}
                source={Images.popup.calBT} />
              <Text>{marker.cal}</Text>
            </View>
          </View>
          <TouchableOpacity style={marker.bookingURL ? styles.bookingmapbar : {display: 'none'}}
            onPress={() => this.openURL(marker.bookingURL)}>
            <Image style={styles.bookingButton}
              source={Images.popup.bookingBT} />
          </TouchableOpacity>
          <ScrollView contentContainerStyle={(marker.body || marker.road) ? styles.description : { display: 'none'}}>
            <Text style={marker.body ? styles.descriptionText : { display: 'none'}}>{marker.body}</Text>
            <Text style={marker.road ? styles.descriptionText : { display: 'none'}}>{"\n"}FOLLOW THE CAMINO:{"\n"}{marker.road}</Text>
          </ScrollView>
        </View>
    );
  }
}

const baseStyle = {
  width: '100%',
  height: 250
};

const styles = StyleSheet.create({
  modalContent: {
    width: Dimensions.get('window').width * 0.95,
    maxHeight: Dimensions.get('window').height * 0.85,
    backgroundColor: '#ffffff',
    borderRadius: 5,
    padding: 0,
  },
  modalHeader: {
    paddingTop: 0,
    paddingBottom: 5,
    flexDirection: 'column',
    alignItems: 'center',
    //backgroundColor: 'red'
    //position: 'absolute'
  },
  headerTitle: {
    fontSize: 16,
    fontWeight: 'bold',
    paddingLeft: 5,
    paddingRight: 5
  },
  headerSubtitle: {

  },
  img: {
    flex: 1,
    //paddingLeft: 20,
    height: null
  },
  img: baseStyle,
  imgWrapper: {
    overflow: 'hidden',
    ...baseStyle,
  },
  serviceView: {
    flexDirection: 'row',
    padding: 4,
    justifyContent: 'space-between',
  },
  serviceIconGroup: {
    flexDirection: 'row',
  },
  serviceIcon: {
    height: 28,
    width: 28,
    marginRight: 0
  },
  serviceIconDisable: {
    height: 28,
    width: 28,
    marginRight: 0,
    opacity: 0.1
  },
  practicalView: {
    flex: 0,
    flexDirection: 'row',
    alignSelf: 'flex-start',
    padding: 4,
    justifyContent: 'space-between',
  },
  practicalIconGroup: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center'
  },
  practicalIcon: {
    height: 48,
    width: 48,
    marginRight: 0
  },
  bookingmapbar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 20
  },
  bookingButton: {
    height: 60,
    width: 60,
    marginRight: 5,
    marginLeft: 5,
    flex: 1
  },
  description: {
    padding: 5,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    //marginBottom: 60
  },
  descriptionText: {
    //flex: 1
  }
});

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';

export default class Button extends Component<{}> {
  constructor(props){
    super(props);

  }
  render() {
    const { text, onPress } = this.props;
    return (
      <View
        style={styles.button}>
        <Text
          onPress={onPress}
          style={styles.text}>
          {text}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    width: '100%',
    padding: 10,
    backgroundColor: 'white',
    paddingLeft: 30,
    borderBottomWidth: 0.5,
    borderColor: 'black'
  },
  text: {
    fontSize: 20,
    color: 'black',
  }
});

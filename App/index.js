/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import { Navigation } from 'react-native-navigation';
import { Component } from 'react';
import { registerScreens } from './Scenes';
import { Colors, Metrics, Images, ApplicationStyles } from './Themes';
registerScreens();
const navigatorStyle = {
  drawUnderNavBar: false,
  navBarTranslucent: false,
  navBarHidden: false,
  topBarElevationShadowEnabled: true,
  navBarLeftButtonColor: Colors.hamburger,
  navBarTextColor: Colors.hamburger,
  navBarBackgroundColor: Colors.snow,
  navBarButtonColor: Colors.hamburger,
  statusBarTextColorScheme: 'light',
  statusBarTextColorSchemeSingleScreen: 'dark'
};
const navigatorButtons = {
  leftButtons: [
    {
      id: 'sideMenu',
      buttonColor: Colors.hamburger,
      icon: Images.menu
    }
  ]
}

class App extends Component {
  constructor(props) {
    super(props);

    this.startApp();
    console.ignoredYellowBox = ['Setting a timer'];

  }
  startApp() {
    Navigation.startSingleScreenApp({
      screen: {
        screen: 'home',
        //titleImage: require('./Scenes/assets/icons/drawerHeaderIcon.png'),
        title: 'Camino Francés',
        navigatorStyle: navigatorStyle,
        navigatorButtons: navigatorButtons
      },
      drawer: {
        left: {
          screen: 'drawer',
          passProps: {}
        }
      },
      type: 'TheSideBar',
      animationType: 'fade'
    });
  }
}

export default App;

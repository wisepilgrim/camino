import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Button,
  ProgressBarAndroid,
  ProgressViewIOS,
  TouchableOpacity,
  Image
} from 'react-native';
import { Colors, Metrics, Images, ApplicationStyles } from '../Themes';
const navigatorStyle = {
  drawUnderNavBar: false,
  navBarTranslucent: false,
  navBarHidden: false,
  topBarElevationShadowEnabled: true,
  navBarLeftButtonColor: Colors.hamburger,
  navBarTextColor: Colors.hamburger,
  navBarBackgroundColor: Colors.snow,
  navBarButtonColor: Colors.hamburger,
  statusBarTextColorScheme: 'light',
  statusBarTextColorSchemeSingleScreen: 'dark'
};

export default class Other extends Component<{}> {
  constructor(props){
    super(props);
    this.state = {

    }
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
  }

  onNavigatorEvent = (event) => {
    switch(event.id){
        case 'sideMenu':
        this.props.navigator.toggleDrawer({
          side: 'left',
          animated: true
        });
        break;
    }
  }

  casa = () => {
    this.props.navigator.handleDeepLink({
      link: 'stuff.casa',
      payload: 'Casa'
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.TouchableOpacity} onPress={this.casa}>
            <Image
              style={styles.opacityImage}
              source={Images.drawer.casaivar}
            />
        </TouchableOpacity>
        <TouchableOpacity style={styles.TouchableOpacity} onPress={this.casa}>
            <Image
              style={styles.opacityImage}
              source={Images.drawer.stoneboat}
            />
        </TouchableOpacity>
        <TouchableOpacity style={styles.TouchableOpacity} onPress={this.casa}>
            <Image
              style={styles.opacityImage}
              source={Images.drawer.thebigmap}
            />
        </TouchableOpacity>
        <TouchableOpacity style={styles.TouchableOpacity} onPress={this.thebigmap}>
            <Image
              style={styles.opacityImage}
              source={Images.drawer.thebook}
            />
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //width: null,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#ffffff',
    //marginTop: 30,
    //marginLeft: 30
  },
  group1: {
    flex: 1/6,
    flexDirection: 'column',
    //backgroundColor: 'red',
    justifyContent: 'flex-start'
  },
  group2: {
    flex: 2/7,
    flexDirection: 'column',
    //backgroundColor: 'red',
    justifyContent: 'flex-start'
  },
  group3: {
    flex: 2/6,
    flexDirection: 'column',
    //backgroundColor: 'red',
    justifyContent: 'flex-start'
  },

  headerImage: {
    resizeMode: 'contain',
    //alignSelf: 'center',
    flex: 1,
    //width: null,
    height: null,
    width: 180,
    marginTop: 30
  },
  TouchableOpacity: {
    //alignSelf: 'center'

    //alignSelf: 'flex-end'
    flex: 1
  },

  opacityImage: {
    resizeMode: 'contain',
    // height: 44,
    // width: 180,
    height: null,
    flex: 1,
    width: 180
  },

});

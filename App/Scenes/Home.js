/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  Image,
  TextInput
} from 'react-native';
import MapView, { UrlTile, LocalTile, MAP_TYPES, Overlay } from 'react-native-maps';
var _ = require('lodash');
import RNFS, { MainBundlePath, DocumentDirectoryPath } from 'react-native-fs';
import ployline from '../Constants/polyline';
import MARKERS from '../Constants/markers';
let INIT_LAT = 43.162547;//42.6036635;
let INIT_LON = -1.237435;//-5.612410;
let INIT_DELTA = 0.006;//0.8//8.0;
let ZOOM_MIN = 1;
let ZOOM_MAX = 16.48;
let BOUND_SW = [42.191229893769624,-8.77914212453792];
let BOUND_NE = [43.235934457948737,-1.0107332326717526];
let ASSET_URL = RNFS.ExternalDirectoryPath + "/Tiles/{z}/{x}/{y}.jpg";
let ASSET_URL_IOS = RNFS.DocumentDirectoryPath + "/Tiles/{z}/{x}/{y}.jpg";
import { Colors, Metrics, Images, ApplicationStyles } from '../Themes';

let oldRegionZoom = INIT_DELTA;
const navigatorStyle = {
  drawUnderNavBar: false,
  navBarTranslucent: false,
  navBarHidden: false,
  topBarElevationShadowEnabled: true,
  navBarLeftButtonColor: Colors.hamburger,
  navBarTextColor: Colors.hamburger,
  navBarBackgroundColor: Colors.snow,
  navBarButtonColor: Colors.hamburger,
  statusBarTextColorScheme: 'light',
  statusBarTextColorSchemeSingleScreen: 'dark'
};

export default class Home extends Component<{}> {


  constructor(props){
    super(props);
    this.state = {
      region: {
        latitude: INIT_LAT,
        longitude: INIT_LON,
        latitudeDelta: INIT_DELTA,
        longitudeDelta: INIT_DELTA
      },
      zoomLevel: {
        min: ZOOM_MIN,
        max: ZOOM_MAX,
      },
      download: {
        status: false
      },
      showUserLocation: false,
      markers : MARKERS
    }

    i = 0;
    index = 0;
    ploylineKey = Object.keys(ployline)

    this.props.navigator.setTitle({
      title: "THE CAMINO FRANCÉS"
    });

    this.onRegionChangeDebounced= _.debounce(this.onRegionChange, 300);
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
  }

  navigate = (screen,title) => {
    this.props.navigator.pop({
      animated: false
    });

    this.props.navigator.push({
      screen: screen,
      title: title,
      navigatorStyle: navigatorStyle,
      navigatorButtons: {
        leftButtons: [
          {
            id: 'sideMenu',
            buttonColor: Colors.hamburger,
            icon: Images.menu
          }
        ]
      },
      animationType: 'none'
    });

    this.props.navigator.toggleDrawer({
      side: 'left',
      animated: false
    });
  }

  onNavigatorEvent = (event) => {
    if(event.type == "DeepLink"){
      if(event.link == 'stuff.casa'){
        this.props.navigator.push({
          screen: 'stuff.casa',
          title: "Casa",
          navigatorStyle: navigatorStyle,
          navigatorButtons: {
            leftButtons: [
              {
                id: 'back',
                buttonColor: Colors.hamburger
              }
            ]
          },
          animationType: 'none'
        });
      } else {
          this.navigate(event.link, event.payload);
      }
    }

    switch(event.id){
        case 'location':
          this.showMyLocation();
        break;
        case 'sideMenu':
        this.props.navigator.toggleDrawer({
          side: 'left',
          animated: true
        });
        break;
    }
  }

  componentWillMount(){
    if(Platform.OS === "android") {
      this.readDirectory(RNFS.ExternalDirectoryPath);
    } else {
      this.readDirectory(RNFS.CachesDirectoryPath);
    }
    this.watchId = navigator.geolocation.getCurrentPosition(
       (position) => {
         const {latitude, longitude} = position.coords;
         if(latitude > BOUND_SW[0] && latitude < BOUND_NE[0]){
           if(longitude > BOUND_SW[1] && longitude < BOUND_NE[1]){
             this.setState({
               showUserLocation: true
             });
           }
         }
      }, (error) => {
        console.log(`error${JSON.stringify(error)}`);
      },
      { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 }
    );
  }

  readDirectory = (path) => {
    RNFS.readDir(path)
      .then((result) => {
        let count = 0;
        for(i=0;i<result.length;i++){
            if(result[i].name === "Tiles"){
              count++;
              break;
            }
        }
        if(count > 0){
          this.setState({
            download: {
              status: true
            }
          });
        }
      });
  }

  componentDidMount(){
    // TODO DO NOT CHANGE THIS FUNCTION.  IT IS THE ONLY THING MAKING IOS ZOOM LEVELS WORK
    // fix for iOS, which fails to load initialRegion correctly at fixed zoom levels
    setTimeout(() => {
      this.mapView && this.mapView.animateToRegion(this.state.region, 500);
      this.state.zoomLevel.min = 8
    }, 500);
  }

  componentWillUnmount(){
    navigator.geolocation.clearWatch(this.watchId);
    this.onRegionChangeDebounced.cancel();
  }

  onRegionChange = (region) => {
    // FINE TUNING OF THIS FUNCTION - It now does not run when zooming in on map.
    //if (region.latitudeDelta >= oldRegionZoom) {
      this.setState({
        region: region
      });

      oldRegionZoom = this.state.region.latitudeDelta;
    //}
  }

  showMyLocation = () => {
    navigator.geolocation.getCurrentPosition(
      (position) => {

        let userregion = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: 0.00922,
            longitudeDelta: 0.00421
        }
        this.map.animateToRegion(userregion, 500);
        this.setState({
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: 0.00922,
            longitudeDelta: 0.00421
          }

        })
        // const {latitude, longitude} = position.coords;
        // alert(position.coords);
        // if(latitude && longitude ){
        //   this.setState({
        //     region: {
        //       latitude: latitude,
        //       longitude: longitude,
        //       latitudeDelta: 0.05,
        //       longitudeDelta: 0.05,
        //     }
        //   });
        // }
     }, (error) => {
       console.log(`error${JSON.stringify(error)}`);
     }
   );
  }

  popupMarker = (event) => {
    console.log(event.nativeEvent);
    /*this.props.navigator.showLightBox({
       screen: 'popup',
       passProps: {
         marker: MARKERS[10].node
       },
       style: {
         backgroundBlur: 'dark',
         tapBackgroundToDismiss: true
       }
     });*/
  }

  openURL = (url) => {
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        console.log("Don't know how to open URI: " + url);
      }
    });
  }

  fetchMarkers = () => {
    var that = this;
    const { markers, region } = this.state;
    return markers.map(function(item) {
          if(item.node.latitude >=  region.latitude -  region.latitudeDelta
            && item.node.latitude <= region.latitude + region.latitudeDelta
            && item.node.longitude >= region.longitude - region.longitudeDelta
            && item.node.longitude <= region.longitude + region.longitudeDelta ){
              if(item.node.marker=='Alto.png') png = Images.markers.alto;
              else if(item.node.marker=='ProvinceBorder.png') png = Images.markers.provinceBorder;
              else if(item.node.marker=='Apartment.png') png = Images.markers.apartment;
              else if(item.node.marker=='Association.png') png = Images.markers.association;
              else if(item.node.marker=='Bombeiro.png') png = Images.markers.bombeiro;
              else if(item.node.marker=='Camping.png') png = Images.markers.camping;
              else if(item.node.marker=='CasaRural.png') png = Images.markers.casaRural;
              else if(item.node.marker=='City.png') png = Images.markers.city;
              else if(item.node.marker=='Closed.png') png = Images.markers.closed;
              else if(item.node.marker=='Hostal.png' || item.node.marker=='Pazo.png') png = Images.markers.hostal;
              else if(item.node.marker=='HotelRural.png') png = Images.markers.hotelRural;
              else if(item.node.marker=='Hotel.png') png = Images.markers.hotel;
              else if(item.node.marker=='Municipal.png') png = Images.markers.municipal;
              else if(item.node.marker=='Parador.png') png = Images.markers.parador;
              else if(item.node.marker=='Parochial.png') png = Images.markers.parochial;
              else if(item.node.marker=='Pension.png') png = Images.markers.pension;
              else if(item.node.marker=='Polideportivo.png') png = Images.markers.polideportivo;
              else if(item.node.marker=='Private.png') png = Images.markers.private;
              else if(item.node.marker=='Xunta.png') png = Images.markers.xunta;
              // Snuck in a few more for additional marker logic below.
              else if(item.node.poimarker=='POI.png') png = Images.markers.poi;
              else if(item.node.poimarker=='Castle.png') png = Images.markers.castle;
              else if(item.node.poimarker=='Ermita.png') png = Images.markers.ermita;
              else if(item.node.poimarker=='Church.png') png = Images.markers.church;
              else if(item.node.poimarker=='Cathedral.png') png = Images.markers.cathedral;
              else if(item.node.poimarker=='Monument.png') png = Images.markers.monument;
              else if(item.node.poimarker=='Museum.png') png = Images.markers.museum;
              else if(item.node.poimarker=='Bridge.png') png = Images.markers.bridge;
              else if(item.node.poimarker=='Castle.png') png = Images.markers.castle;
              else if(item.node.poimarker=='Cruceiro.png') png = Images.markers.cruceiro;
              else if(item.node.poimarker=='Fountain.png') png = Images.markers.fountain;
              else if(item.node.poimarker=='Ruin.png') png = Images.markers.ruin;
              else if (item.node.poimarker=='Notice.png') png = Images.markers.notice;
              else console.log(item.node.title);
              let img = Images.heroImage;

              // THERE ARE THREE MAJOR CATEGORIES OF MAP MARKERS
              // 1: ACCOMMODATION - STRAIGHTFORWARD, USE PNG FROM ABOVE
              if(item.node.nodetype=='Accommodation') {
                return (
                        <MapView.Marker
                          anchor={{x: 0.5, y: 1}}
                          centerOffset={{x: 0, y: -25}}
                          key={i++}
                          image={png}
                          coordinate= {{
                            latitude: Number(item.node.latitude),
                            longitude: Number(item.node.longitude)
                          }}
                          zIndex={0}
                          onPress={() =>
                            that.props.navigator.showLightBox({
                               screen: 'popup',
                               passProps: {
                                 marker: item.node
                               },
                               style: {
                                 backgroundBlur: 'dark',
                                 tapBackgroundToDismiss: true
                               }
                             })
                          }

                          >
                        </MapView.Marker>
                );
              // 2. CITY - EITHER GREEN DOT (NO DISTANCE INFORMATION) OR VIEW BOX WITH DISTANCE
              } else if(item.node.nodetype=='City') {
                // THIS IS THE VIEW BOX
                if(item.node.tohere) {
                  return (
                          <MapView.Marker
                            key={i++}
                            coordinate= {{
                              latitude: Number(item.node.latitude),
                              longitude: Number(item.node.longitude)
                            }}
                            zIndex={1}
                            style={styles.specialMarker}
                            onPress={() =>
                              that.props.navigator.showLightBox({
                                 screen: 'popupother',
                                 passProps: {
                                   marker: item.node
                                 },
                                 style: {
                                   backgroundBlur: 'dark',
                                   tapBackgroundToDismiss: true
                                 }
                               })
                            }
                            >
                            <View style={styles.bubblecontainer}>
                              <View style={styles.bubble}>
                                <Text style={styles.toheretext}>{item.node.tohere}km to {item.node.title}</Text>
                              </View>
                              <View style={styles.arrowBorder} />
                              <View style={styles.arrow} />
                            </View>
                          </MapView.Marker>
                  );
                } else {
                  // THIS IS THE GREEN DOT
                  return (
                          <MapView.Marker
                            key={i++}
                            image={png}
                            coordinate= {{
                              latitude: Number(item.node.latitude),
                              longitude: Number(item.node.longitude)
                            }}
                            zIndex={1}
                            onPress={() =>
                              that.props.navigator.showLightBox({
                                 screen: 'popupother',
                                 passProps: {
                                   marker: item.node
                                 },
                                 style: {
                                   backgroundBlur: 'dark',
                                   tapBackgroundToDismiss: true
                                 }
                               })
                            }
                            >
                          </MapView.Marker>
                  );
                }
              } else {
                // 3. POIs - THESE ALL HAVE THEIR OWN COLLECTION OF PNG
                if(item.node.tohere) {
                  return (
                          <MapView.Marker
                            key={i++}
                            coordinate= {{
                              latitude: Number(item.node.latitude),
                              longitude: Number(item.node.longitude)
                            }}
                            zIndex={1}
                            style={styles.specialMarker}
                            onPress={() =>
                              that.props.navigator.showLightBox({
                                 screen: 'popupother',
                                 passProps: {
                                   marker: item.node
                                 },
                                 style: {
                                   backgroundBlur: 'dark',
                                   tapBackgroundToDismiss: true
                                 }
                               })
                            }
                            >
                            <View style={styles.bubblecontainer}>
                              <Image source={png}/>
                              <View style={styles.bubble}>
                                <Text style={styles.toheretext}>{item.node.tohere}km to {item.node.title}</Text>
                              </View>
                            </View>
                          </MapView.Marker>
                  );
                } else {
                  // THIS IS THE PLAIN MARKER
                  return (
                          <MapView.Marker
                            key={i++}
                            image={png}
                            coordinate= {{
                              latitude: Number(item.node.latitude),
                              longitude: Number(item.node.longitude)
                            }}
                            zIndex={1}
                            onPress={() =>
                              that.props.navigator.showLightBox({
                                 screen: 'popupother',
                                 passProps: {
                                   marker: item.node
                                 },
                                 style: {
                                   backgroundBlur: 'dark',
                                   tapBackgroundToDismiss: true
                                 }
                               })
                            }
                            >
                          </MapView.Marker>
                  );
                }
              };



          } else {
            return null;
          }
      });
  }

  render() {
    const { region, zoomLevel, showUserLocation, download, markers } = this.state;
    let jsonData = this.fetchMarkers();
    road = ploylineKey.map(function(item) {
          index ++
          if(item == "MainRoute") strokeColor = "red"
          else strokeColor = "green"
          return (
                  <MapView.Polyline
                    key={i++}
                    coordinates={ployline[item]}
                    strokeColor={strokeColor}
                    strokeWidth={8}
                    lineDashPattern={[1,12]}
                    lineCap="round"
                    lineJoin="round"
                  />
              );
    });

    return (
      <View style={styles.container}>
          <MapView
              style={styles.map}
              ref={(map) => { this.map = map; }}
              mapType={Platform.OS === "android" ? "none" : MAP_TYPES.STANDARD}
              initialRegion={this.state.region}
              onRegionChange={this.onRegionChangeDebounced}
              minZoomLevel={zoomLevel.min}
              maxZoomLevel={zoomLevel.max}
              showsMyLocationButton={false}
              showsUserLocation={showUserLocation}
              rotateEnabled={false}
            >
            {road}
            {jsonData}
              <LocalTile
               pathTemplate={Platform.OS == "android" ? ASSET_URL: ASSET_URL_IOS}
               tileSize={256}
               zIndex={-1}
              />
          </MapView>
          {
            showUserLocation ?
            <TouchableOpacity
              style={{ position: 'absolute', right: 10, bottom: 10 }}
              onPress={this.showMyLocation}>
              <Image
                source={Images.location} />
            </TouchableOpacity>
            : null
          }
      </View>
    );
  }
}

const baseStyle = {
  width: '100%',
  height: 250,
  borderTopLeftRadius: 0,
  borderTopRightRadius: 0,
  borderBottomLeftRadius: 0,
  borderBottomRightRadius: 0,
};

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: '100%',
    width: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingTop: 500
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    flex: 1,
    zIndex: -1
  },
  buttonWrapper: {
    position: 'absolute',
    top: 10,
    right: 10
  },
  mapButton: {
     position: 'absolute',
     top: 10,
     right: 10,
     width: 32,
     height: 32,
     borderRadius: 25,
     backgroundColor: 'rgba(255, 255, 255, 0.9)',
     justifyContent: 'center',
     alignItems: 'center',
     zIndex: 1,
  },
  image: {
    width: 32,
    height: 32
  },
  instructions: {
    zIndex: 1
  },
  customView: {
    width: 250,
    height: 250,
    backgroundColor: Colors.snow
  },
  bookingmapbar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 20
  },
  bookingButton: {
    height: 60,
    width: 60,
    marginRight: 5,
    marginLeft: 5,
    flex: 1
  },
  modalContent: {
    //flex: 1,
    marginTop: 0,
    paddingLeft: 10,
    paddingRight: 10,
    marginBottom: 0,
    backgroundColor: '#ffffff',//'#eeeeee',
    padding: 0,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    //borderColor: 'black',
  },
  modalHeader: {
    paddingTop: 5,
    paddingBottom: 5,
    flexDirection: 'column',
    alignItems: 'center',
    //backgroundColor: 'red'
    //position: 'absolute'
  },
  headerTitle: {
    fontSize: 14,
    fontWeight: 'bold'
  },
  headerSubtitle: {

  },
  img: baseStyle,
  imgWrapper: {
    overflow: 'hidden',
    ...baseStyle,
  },
  serviceView: {
    flexDirection: 'row',
    alignSelf: 'flex-start',
    padding: 4,
  },
  serviceIconGroup: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  serviceIcon: {
    height: 30,
    width: 30,
    marginRight: 0
  },
  serviceIconDisable: {
    height: 30,
    width: 30,
    marginRight: 0,
    opacity: 0.1
  },
  practicalView: {
    flex: 0,
    flexDirection: 'row',
    alignSelf: 'flex-start',
    padding: 4,
    justifyContent: 'space-between',
  },
  practicalIconGroup: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center'
  },
  practicalIcon: {
    height: 48,
    width: 48,
    marginRight: 0
  },
  bookingmapbar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 20
  },
  bookingButton: {
    height: 60,
    width: 60,
    marginRight: 5,
    marginLeft: 5,
    flex: 1
  },
  description: {
    padding: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    //marginBottom: 60
  },
  descriptionText: {
    //flex: 1
  },
  bubblecontainer: {
  flexDirection: 'column',
  alignSelf: 'flex-start',
   alignItems: 'center',
},
  bubble: {
  flex: 0,
  flexDirection: 'row',
  alignSelf: 'flex-start',
  backgroundColor: 'black',
  padding: 2,
  borderRadius: 2,
  borderColor: 'black',
  borderWidth: 0.5,
},
toheretext: {
  color: '#FFFFFF',
  fontSize: 18,
},
arrow: {
  backgroundColor: 'transparent',
  borderWidth: 4,
  borderColor: 'transparent',
  borderTopColor: 'black',
  alignSelf: 'center',
  marginTop: -9,
},
arrowBorder: {
  backgroundColor: 'transparent',
  borderWidth: 4,
  borderColor: 'transparent',
  borderTopColor: 'black',
  alignSelf: 'center',
  marginTop: -0.5,
},
specialMarker: {
    zIndex: 1,
},
});

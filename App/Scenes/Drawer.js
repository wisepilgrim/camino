import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity
} from 'react-native';
import { Colors, Metrics, Images, ApplicationStyles } from '../Themes';
import Button from '../Components/Button';

export default class Drawer extends Component<{}> {
  constructor(props){
    super(props);
  }

  home = () => {
    this.props.navigator.handleDeepLink({
      link: 'home',
      payload: 'Camino Francés'
    });
  }

  offline = () => {
    this.props.navigator.handleDeepLink({
      link: 'offline',
      payload: 'Download Map Tiles'
    });
  }

  about = () => {
    this.props.navigator.handleDeepLink({
      link: 'about',
      payload: 'About'
    });
  }

  legend = () => {
    this.props.navigator.handleDeepLink({
      link: 'legend',
      payload: 'Map Legend'
    });
  }

  elevation = () => {
    this.props.navigator.handleDeepLink({
      link: 'elevation',
      payload: 'Elevation Profile'
    });
  }
  
  other = () => {
    this.props.navigator.handleDeepLink({
      link: 'other',
      payload: 'Other Stuff'
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.group1}>
          <Image style={styles.headerImage} source={Images.drawer.drawerHeader} />
        </View>
        <View style={styles.group2}>
          <TouchableOpacity style={styles.TouchableOpacity} onPress={this.home}>
              <Image
                style={styles.opacityImage}
                source={Images.drawer.offlinemap}
              />
          </TouchableOpacity>
          <TouchableOpacity style={styles.TouchableOpacity} onPress={this.home}>
              <Image
                style={styles.opacityImage}
                source={Images.drawer.onlinemap}
              />
          </TouchableOpacity>
          <TouchableOpacity style={styles.TouchableOpacity} onPress={this.elevation}>
              <Image
                style={styles.opacityImage}
                source={Images.drawer.elevationProfile}
              />
          </TouchableOpacity>
          <TouchableOpacity style={styles.TouchableOpacity} onPress={this.legend}>
              <Image
                style={styles.opacityImage}
                source={Images.drawer.about}
              />
          </TouchableOpacity>
          <TouchableOpacity style={styles.TouchableOpacity} onPress={this.offline}>
              <Image
                style={styles.opacityImage}
                source={Images.drawer.starthere}
              />
          </TouchableOpacity>
        </View>
        <View style={styles.group3}>
          <TouchableOpacity style={styles.TouchableOpacity} onPress={this.other}>
              <Image
                style={styles.opacityImage}
                source={Images.drawer.otherstuff}
              />
          </TouchableOpacity>
        </View>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //width: null,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#ffffff',
    //marginTop: 30,
    //marginLeft: 30
  },
  group1: {
    flex: 1/6,
    flexDirection: 'column',
    //backgroundColor: 'red',
    justifyContent: 'flex-start'
  },
  group2: {
    flex: 2/7,
    flexDirection: 'column',
    //backgroundColor: 'red',
    justifyContent: 'flex-start'
  },
  group3: {
    flex: 3/8,
    flexDirection: 'column',
    //backgroundColor: 'red',
    justifyContent: 'flex-start'
  },

  headerImage: {
    resizeMode: 'contain',
    //alignSelf: 'center',
    flex: 1,
    //width: null,
    height: null,
    width: 180,
    marginTop: 30
  },
  TouchableOpacity: {
    //alignSelf: 'center'

    //alignSelf: 'flex-end'
    flex: 1
  },

  opacityImage: {
    resizeMode: 'contain',
    // height: 44,
    // width: 180,
    height: null,
    flex: 1,
    width: 180
  },

});

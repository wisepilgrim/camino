import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Button,
  ProgressBarAndroid,
  ProgressViewIOS,
} from 'react-native';
import { Colors, Metrics, Images, ApplicationStyles } from '../Themes';
var about = require('../Constants/About.json');
import { zip, unzip, unzipAssets, subscribe } from 'react-native-zip-archive';
import RNFS, { MainBundlePath, DocumentDirectoryPath } from 'react-native-fs';
let ZIP_FILE = "Tiles.zip";
//let TILE_URL = "https://s3.eu-west-2.amazonaws.com/sleeponthecamino/Tiles12WEBP.zip";

// FOR NOW, TWO DIFFERENT DOWNLOADS SINCE WEBP DOES NOT WORK ON IOS
let TILE_URL_IOS = "https://s3.eu-west-2.amazonaws.com/sleeponthecamino/Tiles.zip";
let TILE_URL = "https://s3.eu-west-2.amazonaws.com/sleeponthecamino/Tiles.zip";
//"https://s3.eu-west-2.amazonaws.com/sleeponthecamino/TilesSJPP.zip";
//"https://s3.eu-west-2.amazonaws.com/sleeponthecamino/Tileswebp.zip";

export default class Offline extends Component<{}> {
  constructor(props){
    super(props);
    this.state = {
      download: {
        status: false,
        progress: -1,
        message: "I AGREE - Download"
      }
    }
    this.props.navigator.setTitle({
      title: "Download Map Tiles"
    });
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
  }

  onNavigatorEvent = (event) => {
    switch(event.id){
        case 'sideMenu':
        this.props.navigator.toggleDrawer({
          side: 'left',
          animated: true
        });
        break;
    }
  }

  componentWillMount(){

  }

  componentWillUnmount(){
    //this.unZipProgress.remove();
  }

  componentDidMount()  {
    /*if(Platform.OS === "android") {
      this.readDirectory(RNFS.ExternalDirectoryPath);
    } else {
      this.readDirectory(RNFS.CachesDirectoryPath);
    }*/
  }

  download = () => {
      this.setState({
        download: {
          status: true,
          message: "Downloading",
          progress: 0
        }
      });
     if(Platform.OS === "android") {
       this.copyFromURL(RNFS.ExternalDirectoryPath);
     } else {
       this.copyFromURL(RNFS.DocumentDirectoryPath);
     }
  }

  readDirectory = (path) => {
    RNFS.readDir(path)
      .then((result) => {
        let count = 0;
        for(i=0;i<result.length;i++){
            if(result[i].name === "Tiles"){
              count++;
              break;
            }
        }
        if(count > 0){
          this.setState({
            download: {
              status: true,
              progress: 100,
              message: "Done"
            }
          });
        }
      });
  }

  copyFromAsset = () => {
      RNFS.getAllExternalFilesDirs()
        .then((dirs) => {
          var path = dirs[0] + "/myfile.zip";
          RNFS.copyFileAssets("mytiles.zip",path)
            .then((result) => {
                unzip(path, dirs[0]).then((done) => {
                  if(RNFS.exists(path)){
                    RNFS.unlink(path);
                  }
                });
            });
      });
  }

  copyFromURL = (path) => {
      RNFS.downloadFile({
          fromUrl: (Platform.OS == "android") ? TILE_URL : TILE_URL_IOS,
          //fromUrl: TILE_URL,
          toFile: path + "/" + ZIP_FILE,
          progressDivider: 0,
          background: true,
          begin: (res) => {
            this.setState({
              download: {
                status: true,
                message: "Downloading",
                progress: 0
              }
            })
          },
          progress: (res) => {
            var progress = Math.round(res.bytesWritten / res.contentLength * 100);
            this.setState({
              download: {
                status: true,
                progress: progress,
                message: "Downloading"
              }
            })
          }
        }).promise.then((result) => {
          if(result.statusCode === 200){
            this.setState({
              download: {
                status: true,
                progress: 100,
                message: "Unpacking"
              }
            });

            let filePath = path + "/"+ ZIP_FILE;
            this.unZipProgress = subscribe(({progress, filePath}) => {
              this.setState({
                download: {
                  status: true,
                  progress: Math.round(progress * 100),
                  message: "Unpacking"
                }
              });
            });

            unzip(filePath, path)
              .then((done) => {
                RNFS.unlink(path + "/"+ ZIP_FILE);
                this.setState({
                  download: {
                    status: true,
                    progress: 100,
                    message: "Done"
                  }
                });
                this.unZipProgress.remove();
            });
          }
        }).catch((error) => {
          this.setState({
            download: {
              status: false,
              progress: -1,
              message: "Failed - Retry ?"
            }
          });
          if (RNFS.isResumable(1)) {
              RNFS.resumeDownload(1)
          }
        });
  }

  render() {
    const { download } = this.state;
    let buttonTitle = download.message;
    return (
      <View style={styles.container}>
        <ScrollView contentContainerStyle={styles.scrollview}>
        <Text style={styles.title}>
          {about.downloading.title}
        </Text>
        <Text style={styles.text}>
          {about.downloading.text}
        </Text>
        <Button
          style={styles.button}
          onPress={this.download}
          title= {buttonTitle}
          color={Colors.fire}
          disabled={download.status}>
        </Button>
        {
          download.progress >= 0 && download.progress < 100 ?
            <View style={styles.progressView}>
              <Text style={styles.progressText}>
                {download.message + " - " +
                 download.progress + "%"}
              </Text>
              {
                Platform.OS === "android" ?
                  <ProgressBarAndroid
                    style={styles.progress}
                    color={Colors.black}
                    styleAttr="Horizontal"
                    progress={download.progress/100}
                  /> :
                  <ProgressViewIOS
                    style={styles.progress}
                    progress={download.progress/100}
                    progressTintColor={Colors.black}
                    progressViewStyle="bar"
                    trackTintColor={Colors.whitish}
                  />
              }
            </View>
            : null
        }
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    alignSelf: 'baseline',
    backgroundColor: Colors.snow
  },
  title: {
    fontSize: 18,

  },
  scrollview: {
    paddingBottom: 100
  },
  text: {
    fontSize: 16,
    textAlign: 'justify',
    justifyContent: 'center',
    marginBottom: 20
  },
  header: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 10,
    textAlign: 'center'
  },
  progressView: {
    flex: 1,
    alignItems: 'center',
    marginTop: 20
  },
  progressText: {
    color: Colors.black,
    fontSize: 22
  },
  progress: {
    width: '100%',
    alignItems: 'center',
    marginTop: 10
  },
  button: {
    backgroundColor: Colors.snow
  }
});

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  Dimensions
} from 'react-native';
var about = require('../Constants/About.json');
import { Colors, Metrics, Images, ApplicationStyles } from '../Themes';

export default class Stoneboat extends Component<{}> {
  constructor(props){
    super(props);
    this.props.navigator.setTitle({
      title: "The Stone Boat"
    });
    global.navigator = this.props.navigator;
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
  }

  onNavigatorEvent = (event) => {
    switch(event.id){
        case 'sideMenu':
        this.props.navigator.toggleDrawer({
          side: 'left',
          animated: true
        });
        break;
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          ref={(view) => this._scrollView = view}
          contentContainerStyle={styles.scrollview}
          showsVerticalScrollIndicator={true}
          alwaysBounceVertical={false}
          scrollEnabled={true}
          bounces={true}
          horizontal={false}
          overScrollMode='always'
          showsHorizontalScrollIndicator={false}
          >
          <View style={styles.innercontainer}>
          <Text style={styles.header}>THE STONE BOAT PLACEHOLDER</Text>

          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.snow
  },
  innercontainer: {
    width: Dimensions.get('window').width ,
  },
  iconset: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginBottom: 16,
    paddingRight: 10
  },
  iconsetvert: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    marginBottom: 16,
    paddingRight: 10
  },
  text: {
    fontSize: 16,
    flex: 1,
    marginLeft: 10
  },
  icon: {
    height: 50,
    width: 50,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    resizeMode: 'contain'
  },
  iconwide: {
    width: 300,
    height: 50,
    resizeMode: 'contain',
    marginLeft: 10,
    marginBottom: 10

  },
  header: {
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 10,
    marginBottom: 30,
    textAlign: 'center',
  },
  scrollview: {
    flexDirection: 'column',
    //height: viewheight,
    alignSelf: 'flex-start',

  },
});

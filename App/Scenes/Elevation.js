import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  Dimensions
} from 'react-native';
var about = require('../Constants/About.json');
import { Colors, Metrics, Images, ApplicationStyles } from '../Themes';

const profileheight = 1000;
const profilewidth = 2568;
const window = Dimensions.get('window');
const viewheight = window.height - 100;

let scaleRatio = viewheight / profileheight;
console.log('aspect is: ' + scaleRatio);

let newprofilewidth = profilewidth*scaleRatio;
let newprofileheight = viewheight;
//let newprofilewidth=imageviewheight*imageaspect;
console.log('window height is: ' + window.height);
console.log('new profile dimensions: ' + newprofilewidth + ' x ' + newprofileheight );

export default class About extends Component<{}> {
  constructor(props){
    super(props);
    this.state = { scroll: 1000, };

    this.props.navigator.setTitle({
      title: "Elevation Profile"
    });

    this.handleScroll = this.handleScroll.bind(this)
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
  }

  onNavigatorEvent = (event) => {
    switch(event.id){
        case 'sideMenu':
        this.props.navigator.toggleDrawer({
          side: 'left',
          animated: true
        });
        break;
    }
  }

  handleScroll = function(event: Object) {
     this.setState({scroll: event.nativeEvent.contentOffset.x});
  }


  componentDidMount(){
    this._scrollView.getScrollResponder().scrollTo({x: this.state.scroll, y: 0, animated: true})
  }


  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          ref={(view) => this._scrollView = view}
          contentContainerStyle={styles.scrollview}
          showsVerticalScrollIndicator={false}
          alwaysBounceVertical={false}
          scrollEnabled={true}
          horizontal={true}
          bounces={false}
          overScrollMode='always'
          onScroll={this.handleScroll}

          >
          <Image style={styles.profileimage}
            source={Images.profiles.profile1} />
          <Image style={styles.profileimage}
            source={Images.profiles.profile2} />
          <Image style={styles.profileimage}
            source={Images.profiles.profile3} />
          <Image style={styles.profileimage}
            source={Images.profiles.profile4} />
          <Image style={styles.profileimage}
            source={Images.profiles.profile5} />
          <Image style={styles.profileimage}
            source={Images.profiles.profile6} />
          <Image style={styles.profileimage}
            source={Images.profiles.profile7} />
          <Image style={styles.profileimage}
            source={Images.profiles.profile8} />
          <Image style={styles.profileimage}
            source={Images.profiles.profile9} />
          <Image style={styles.profileimage}
            source={Images.profiles.profile10} />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'darkgray',
    //justifyContent: 'flex-end'
    //flexDirection: 'row',
    //width: Dimensions.get('window').width * 0.95,
    //height: Dimensions.get('window').height * 0.75,
  },
  scrollview: {
    flexDirection: 'row',
    height: viewheight,
    alignSelf: 'flex-end'
    //bottom: 0
    //marginLeft: 40

    //margin: 10
  },
  profileimage: {
    //height: Dimensions.get('window').height * 0.75,
    width: newprofilewidth,
    height: newprofileheight,
    left: 0,
    flex: 1,
    //resizeMode: 'cover'
    //height: Dimensions.get('window').height * 0.75,
  }
});

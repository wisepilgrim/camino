import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Button,
  ProgressBarAndroid,
  ProgressViewIOS,
  TouchableOpacity,
  Image
} from 'react-native';
import { Colors, Metrics, Images, ApplicationStyles } from '../../Themes';

export default class Casa extends Component<{}> {
  constructor(props){
    super(props);
    this.state = {

    }
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
  }

  onNavigatorEvent = (event) => {
    switch(event.id){
        case 'sideMenu':
        this.props.navigator.toggleDrawer({
          side: 'left',
          animated: true
        });
        break;
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Casa</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //width: null,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#ffffff',
    //marginTop: 30,
    //marginLeft: 30
  }
});

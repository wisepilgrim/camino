import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView
} from 'react-native';
var about = require('../Constants/About.json');
import { Colors, Metrics, Images, ApplicationStyles } from '../Themes';

export default class About extends Component<{}> {
  constructor(props){
    super(props);
    this.props.navigator.setTitle({
      title: "About"
    });
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
  }

  onNavigatorEvent = (event) => {
    switch(event.id){
        case 'sideMenu':
        this.props.navigator.toggleDrawer({
          side: 'left',
          animated: true
        });
        break;
    }
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: Colors.snow }}>
        <ScrollView
          style={{flex: 1, height: '100%', margin: 10}}
          alwaysBounceVertical={true}
          scrollEnabled={true}
          showsVerticalScrollIndicator={true}
          overScrollMode='always'
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={styles.container}>
          <Text style={styles.header}>
            {about.disclaimer.title}
          </Text>
          <Text style={styles.text}>
            {about.disclaimer.text}
          </Text>
          <Text style={styles.header}>
            {about.offline.title}
          </Text>
          <Text style={styles.text}>
            {about.offline.text}
          </Text>
          <Text style={styles.header}>
            {about.contact.title}
          </Text>
          <Text style={styles.text}>
            {about.contact.text}
          </Text>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    fontSize: 16,
    textAlign: 'justify',
    justifyContent: 'center',

  },
  header: {
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 10,
    marginBottom: 10,
    textAlign: 'center'
  }
});

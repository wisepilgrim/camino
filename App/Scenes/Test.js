import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  ProgressBarAndroid,
  ProgressViewIOS,
} from 'react-native';
import { Colors, Metrics, Images, ApplicationStyles } from '../Themes';
import MapView from 'react-native-maps';

export default class Test extends Component<{}> {
  constructor(props){
    super(props);

  }
  componentDidMount()  {

  }
  render(){
    return (
      <View style={{ position: 'relative', height: 500}}>
        <MapView
          style={{ left:0, right: 0, top:0, bottom: 0, position: 'absolute' }}
          initialRegion={{
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 20,
    alignSelf: 'baseline'
  },
  text: {
    fontSize: 16,
    textAlign: 'justify',
    justifyContent: 'center',
    marginBottom: 20
  },
  header: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 10,
    textAlign: 'center'
  },
  progressView: {
    flex: 1,
    alignItems: 'center',
    marginTop: 20
  },
  progressText: {
    color: Colors.black,
    fontSize: 22
  },
  progress: {
    width: '100%',
    alignItems: 'center',
    marginTop: 10
  }
});

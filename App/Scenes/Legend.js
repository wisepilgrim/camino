import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  Dimensions
} from 'react-native';
var about = require('../Constants/About.json');
import { Colors, Metrics, Images, ApplicationStyles } from '../Themes';

export default class About extends Component<{}> {
  constructor(props){
    super(props);
    this.props.navigator.setTitle({
      title: "Map Legend"
    });
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
  }

  onNavigatorEvent = (event) => {
    switch(event.id){
        case 'sideMenu':
        this.props.navigator.toggleDrawer({
          side: 'left',
          animated: true
        });
        break;
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          ref={(view) => this._scrollView = view}
          contentContainerStyle={styles.scrollview}
          showsVerticalScrollIndicator={true}
          alwaysBounceVertical={false}
          scrollEnabled={true}
          bounces={true}
          horizontal={false}
          overScrollMode='always'
          showsHorizontalScrollIndicator={false}
          >
          <View style={styles.innercontainer}>
          <Text style={styles.header}>MAP MARKER LEGEND</Text>

            <View style={styles.iconsetvert}>
              <Image style={styles.iconwide} source={Images.markers.citydistance} />
              <Text style={styles.text}>Distances shown in the black boxes indicate the distance TO that point, from the previously indicated point, along the primary route. It typically marks the distance to a city, but may also indicate the distance to other significant landmarks; in these cases it will be shown beneath one of the icons below. </Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.city} />
              <Text style={styles.text}>Not all places indicate a distance due to their size, particularly in Galicia where every cluster of home becomes a hamlet.  In these cases the town or village is designated with a circle.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.alto} />
              <Text style={styles.text}>Mountain peak or "Alto." These are only shown on the map when the camino passes over them.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.provinceBorder} />
              <Text style={styles.text}>The symbols indicate the border between Spain's Autonomous Communities, and in some cases the border between regions within a Community.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.fountain} />
              <Text style={styles.text}>Water.  Generally only used to indicate a water supply outside of a populated area.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.ermita} />
              <Text style={styles.text}>Ermita or Capillas can be found in the least likely places.  Like the other two purple icons below, they represent a place connected to a religious order.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.church} />
              <Text style={styles.text}>Churches and Monasteries, as well as convents.  Larger than a chapel but smaller than a Cathedral.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.cathedral} />
              <Text style={styles.text}>Cathedral or Basilica.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.monument} />
              <Text style={styles.text}>A monument or landmark of significance, generally with a connection to the history of the camino.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.museum} />
              <Text style={styles.text}>There are a few museums along the camino, some to be expected but others not.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.bridge} />
              <Text style={styles.text}>Bridges are critical to the safe passage of pilgrims.  These markers designate the more sensational ones, either for their characteristics or the legends told about them.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.castle} />
              <Text style={styles.text}>Because castles are cool.  Some are open to the public, most are just the empty shells of a past life of glory without plumbing.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.cruceiro} />
              <Text style={styles.text}>Roadside crosses known as Cruceiros marked the way to Santiago centuries ago.  They are few and far between but almost always have a design worth noting.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.ruin} />
              <Text style={styles.text}>A pile of rocks, sometimes well organized but not always; the ruins of a building of historical significance.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.poi} />
              <Text style={styles.text}>If it doesn't fit into any of the categories above, it goes here.  Generally for places of importance to pilgrims.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.notice} />
              <Text style={styles.text}>NOTICE:  These are meant to grab your attention and direct it to some important option in the trail, or a warning.  Remember that the camino changes frequently due to local constraints and that these notices should NOT be considered the gospel truth.  They were true at one time but things may have changed, as always follow the local signage.</Text>
            </View>
            <Text style={styles.header}>ALBERGUE TYPES</Text>

            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.municipal} />
              <Text style={styles.text}>MUNICIPAL. These albergues are almost always run by the local government. They are exclusively for pilgrims and are usually the least expensive of options. Nearly all have a kitchen. Few accept reservations.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.xunta} />
              <Text style={styles.text}>XUNTA. Similar to a municipal, but unique to Galicia where the government has set up a large network of albergues built largely into former school houses. The services and equipment are standardized and a disposable pillow case and bottom sheet are included.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.private} />
              <Text style={styles.text}>PRIVATE. Privately owned and operated albergues differ from the municipal albergues in that they more often accept reservations, provide service to non-pilgrims, and typically have a higher price. They are often newer with more services (meals etc.), though it is important to note that quite a few do not have a kitchen. They frequently have a bar or restaurant attached.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.association} />
              <Text style={styles.text}>ASSOCIATION. Various associations around the world maintain albergues which are staffed by members (volunteers) on a rotating basis. Several “friends of” associations from around Spain do the same. These typically fall somewhere between Municipal and Private from a price/services standpoint. Because of the way they are run they almost always provide fresh and friendly hospitaleros who are former pilgrims giving back to the community for a few weeks.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.parochial} />
              <Text style={styles.text}>PAROCHIAL. Similar to an albergue run by an association, with the important distinction that the organization in charge is a religious order.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.camping} />
              <Text style={styles.text}>CAMPING. Although not strictly an albergue, several campsites have been included as an inexpensive alternative. Few of these are in town, but all are within a walkable distance.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.polideportivo} />
              <Text style={styles.text}>POLIDEPORTIVO. This is the Spanish version of a sports hall. They are stuffed with whatever bedding is available (if it is available) during periods of extremely high traffic to handle the crowds.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.bombeiro} />
              <Text style={styles.text}>BOMBEIROS VOLUNTARIOS. Shared accommodation located in the local Volunteer Fire Department. Seldom with cooking facilities, but always with an inside scoop on the best place to eat.</Text>
            </View>
            <Text style={styles.header}>PRIVATE ACCOMMODATION</Text>

            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.parador} />
              <Text style={styles.text}>PARADOR. Generally considered to be a luxury hotel chain in Spain, and usually located in a rehabilitated building of historical importance; castles, pilgrim hospitals, and the like.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.hotel} />
              <Text style={styles.text}>HOTEL. The standard hotel class, the range of services and quality vary greatly.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.hostal} />
              <Text style={styles.text}>HOSTAL. A less expensive alternative to a hotel. Hostals typically have a bar or restaurant which offers meals and drinks to both guest and the public alike.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.pension} />
              <Text style={styles.text}>PENSION. A less expensive alternative to a hostal. Pensions typically offer private rooms with shared bathrooms, although this is not always the case.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.hotelRural} />
              <Text style={styles.text}>HOTEL RURAL. Similar in class to a hotel, but typically found in a more rural setting. The distinction between a Hotel Rural and a Casa Rural is often hard to discern.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.casaRural} />
              <Text style={styles.text}>CASA RURAL. Similar in style and service to a B&B and set in a rural setting or small village.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.apartment} />
              <Text style={styles.text}>APARTMENT. Available primarily in larger cities, apartments offer multiple bedrooms as well as a kitchen and are rented as a whole unit making them a good option for small groups.</Text>
            </View>
            <Text style={styles.header}>ALBERGUE SERVICES</Text>

            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.popup.hostelBT} />
              <Text style={styles.text}>Number of Beds / Price</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.popup.openBT} />
              <Text style={styles.text}>Opening time if any.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.popup.closeBT} />
              <Text style={styles.text}>Closing time if any.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.popup.calBT} />
              <Text style={styles.text}>Months open, 'Holy' indicates the week before Easter.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.popup.pilgrimBT} />
              <Text style={styles.text}>Exclusively for pilgrims.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.popup.hotelBT} />
              <Text style={styles.text}>Offers private rooms.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.popup.reserveBT} />
              <Text style={styles.text}>Accepts reservations.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.popup.barrestBT} />
              <Text style={styles.text}>Has a bar or offers meals.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.popup.kitchenBT} />
              <Text style={styles.text}>Has a kitchen for preparing your own meals. May range from fully equipped to microwave only.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.popup.familyBT} />
              <Text style={styles.text}>Meal offered is family style; dinner as a group.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.popup.vegBT} />
              <Text style={styles.text}>Vegetarian options available.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.popup.washerBT} />
              <Text style={styles.text}>Washer and dryer available.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.popup.wifiBT} />
              <Text style={styles.text}>Offers an internet connection, most typically WiFi.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.popup.bikeBT} />
              <Text style={styles.text}>Offers a secure location to store your bike.</Text>
            </View>
            <Text style={styles.header}>CITY SERVICES</Text>

            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.popup.hostelBT} />
              <Text style={styles.text}>Has shared accommodation (albergues).</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.popup.hotelBT} />
              <Text style={styles.text}>Has private accommodation.  May include private rooms in albergues.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.popup.campingBT} />
              <Text style={styles.text}>Campsite</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.popup.hospitalBT} />
              <Text style={styles.text}>Has a medical facility suitable for evaluations. Note that in smaller villages the hours vary by day and season. Inquire at your albergue or hotel where the nearest “Centro de Salud” is.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.popup.pharmacyBT} />
              <Text style={styles.text}>Has a pharmacy. Note that in towns where there is more than one pharmacy, it is common for one to be designated as being “a guardia.” This pharmacy will be open 24h, but may required you to ring a bell.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.popup.barrestBT} />
              <Text style={styles.text}>Has a bar or restaurant, where a meal can be ordered during normal dining hours.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.popup.supermarketBT} />
              <Text style={styles.text}>Has a grocers, or a supermarket in larger towns.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.popup.atmBT} />
              <Text style={styles.text}>Has an ATM</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.popup.correosBT} />
              <Text style={styles.text}>Has a Post Office</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.popup.busterminalBT} />
              <Text style={styles.text}>Has a bus terminal, indicating a range of connecting busses throughout the region or country.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.popup.busBT} />
              <Text style={styles.text}>Has a bus stop, indicating that local bus services are available to surrounding villages.</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.popup.trainBT} />
              <Text style={styles.text}>Has a train station.</Text>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.snow
  },
  innercontainer: {
    width: Dimensions.get('window').width ,
  },
  iconset: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginBottom: 16,
    paddingRight: 10
  },
  iconsetvert: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    marginBottom: 16,
    paddingRight: 10
  },
  text: {
    fontSize: 16,
    flex: 1,
    marginLeft: 10
  },
  icon: {
    height: 50,
    width: 50,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    resizeMode: 'contain'
  },
  iconwide: {
    width: 300,
    height: 50,
    resizeMode: 'contain',
    marginLeft: 10,
    marginBottom: 10

  },
  header: {
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 10,
    marginBottom: 30,
    textAlign: 'center',
  },
  scrollview: {
    flexDirection: 'column',
    //height: viewheight,
    alignSelf: 'flex-start',

  },
});

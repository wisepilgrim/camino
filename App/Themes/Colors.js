const colors = {
  darkPrimary: 'rgba(33,83,97,1)',
  primary: '#4eafc3',
  lightPrimary: '#FFCDD2',
  accent: '#795548',
  background: '#1F0808',
  transparent: 'rgba(0,0,0,0)',
  windowTint: 'rgba(0, 0, 0, 0.4)',
  panther: '#161616',
  charcoal: '#595959',
  coal: '#2d2d2d',
  bloodOrange: '#fb5f26',
  snow: 'white',
  whitish: 'rgba(255,255,255,0.5)',
  underlayColor: 'rgba(0,0,0,0.1)',
  ember: 'rgba(164, 0, 48, 0.5)',
  fire: '#e73536',
  drawer: 'rgba(30, 30, 29, 0.95)',
  eggplant: '#251a34',
  border: '#483F53',
  banner: '#5F3E63',
  text: '#E0D7E5',
  black: '#000000',
  hairline: '#8E8E8E',
  button: '#00b2c6',
  hamburger: 'black'//'#e90006'
};

export default colors;
